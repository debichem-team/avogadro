avogadro-i18n
-------------

avogadroapp requires internationalization sources from avogadro-i18n,
provided separately in upstream git repo
https://github.com/OpenChemistry/avogadro-i18n

By default avogadro/CMakeLists.txt checks for avogadro-i18n in an
adjacent source directory and downloads the upstream git repo if it is
not present.

For Debian we want the build to be able to proceed without external
network access.  avogadro-i18n could be packaged separately. It
contains i18n files for both avogadrolibs and avogadroapp.  However
source for avogadrolibs already contains its own i18n subdirectory and
therefore does not use the separate avogadro-i18n source.

For that reason we simply make avogadro-i18n available via the debian
subdir for avogadro (avogadroapp) rather than packaging separately.
debian/patches/avogadro-i18n.patch patches avogadro/CMakeLists.txt to
use this location.

The easiest way to handle the separate source repo is with a git
subtree. This can pull in the avogadro-i18n source via a single commit
which keeps the commit log clean (but means we cannot directly push
any i18n changes made to it).

The i18n source in debian/avogadro-i18n can be updated with:

git subtree pull --prefix debian/avogadro-i18n https://github.com/OpenChemistry/avogadro-i18n master --squash

Replace "master" with the specific tag id if required.
